// Название модуля
angular.module('User',[])

// Прописываем роуты модуля
    .config(function($routeProvider){
        $routeProvider
            .when('/user/', {
                templateUrl: '/User/main.tpl.html'
            });
    });

// Добавляем User в главный модуль
angular.module('app').requires.push('User');