// Название модуля
angular.module('Search',[])

// Прописываем роуты модуля
    .config(function($routeProvider){
        $routeProvider
            .when('/search/', {
                templateUrl: '/tmp/modules/Search/client/View/search.tpl.html'
            });
    });

// Добавляем Search в главный модуль
angular.module('app').requires.push('Search');