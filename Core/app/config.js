
/*
###############################
#    Глобальные переменные
###############################
*/

PATH_CORE       = __dirname+'/../../Core';
PATH_ADMIN      = __dirname+'/../admin';
PATH_APP        = __dirname+'/';
PATH_MODULES    = __dirname+'/../modules';
PATH_TEMPLATE   = __dirname+'/../template';
PATH_LIBS       = __dirname+'/../libs/server';
PATH_PUBLIC     = __dirname+'/../../public';





var path = require('path');
var fs = require('fs');
var file = require('file-system');
var nconf = require('nconf');
var http = require('http');

var User = require(PATH_APP+'server/Models/User');

var chalk = require('chalk'); // Разукрашиваем консоль

var passport = require('passport');
var session = require('express-session');

//var geo = require('geotools');
//var user = require('../models/userModel');




exports.app = function(express, app) {
    
/*
###############################
#   Developer || Production
###############################
*/
    if(process.argv[2]==('dev' || 'developer' || 'development' || 'd')) 
        app.set('env','development');
    else 
        app.set('env','production');
    
    app.locals.env = app.get('env');
    
/*
###############################
#   Параметры ExpressJS
###############################
*/  
    
    var bodyParser = require('body-parser');
        //app.set('views', path.join(__dirname, '..', 'views'));
    app.set('view engine', 'jade');
    //app.use(express.favicon(__dirname + '/../dist/favicon.png'));
    
    var logger = require('morgan');
    app.use(bodyParser.json());
    
    var methodOverride = require("method-override");
    app.use(methodOverride());
    
    
    //if ('development' == app.get('env')) {
        app.use(express.static(PATH_PUBLIC));
        app.use(logger('dev'));
    //}
    
    app.use(session({
      secret: 'sadf!_dfeWq4s'
    }))
    app.use(passport.initialize());
    app.use(passport.session());


    console.log(chalk.blue.inverse('>') + ' Middleware: ' + chalk.green.bold('OK'));
}
/* не используется */
exports.controller = function(app,path) {
    var path = path || __dirname+'/../controllers';
    fs.readdirSync(path).forEach(function(file) {
        
        if (file.substr(-3) == '.js') {
            route = require(__dirname+'/../controllers/' + file);
            route.controller(app);
        }
    });
}

exports.templateAngular = function(app,path) {
    var path = path || ['**/*.jade','!admin/**/*.jade'];
    file.recurse(PATH_CORE, path, function(filepath, filename) {
        var core = PATH_CORE.split('/');
        var path = filepath.split('/'+core[core.length-1]+'/')[1].split('/');
        var url = '/tmp/'+path.slice(0,-1).join('/')+'/'+filename.split('.jade')[0]+'.tpl.html';
        app.get(url,function(req,res){
            res.render(filepath, {});
        });


    });
    
    
    console.log(chalk.blue.inverse('>') + ' Angular template: ' + chalk.green.bold('OK'));
}
/*
    Парсится build.js. Вытаскиваем из него $routeProvider AngularJS и создаем роуты в expressJS 
*/
exports.route = function(app,route) {
    var self = this;
    fs.readFile(PATH_PUBLIC+'/js/build.js', "utf-8", function (err, data) {
        if (err) throw err;
      
        var matches = data.match(/(\$routeProvider(\s*).when([\s('/{},A-Za-z0-9_:.)]*);)/gi);
        
        for(var i = 0; matches.length > i; i++){
            matches[i].match(/.when\('(.*)'/g).forEach(function(value,key){
                var route = value.split("when('")[1].split("'")[0];
                self.indexPage(app,false,route);
            });
            
        }
    });
}

exports.api = function(app,path,url) {
    var path = path || [
        '**/server/Controllers/*.js'
    ];
    var url = url || '/api/';
    
    var appRoute = function(file,route,filepath){
        app[route.method || 'get'](
               url+file.split('.')[0] + (!!route.params ? '/'+route.params.join('/') : ''),
               route.preload || function(req,res,next){next()},
               route.link || function(req,res){res.send(filepath)});
    }
    // Ищем контроллеры по всему проекту.
    file.recurse(PATH_CORE, path, function(filepath, filename) {
        if (filename) {
            var route = require(filepath);
            !!route.length ? route.forEach(function(o){appRoute(filename,o)}) : appRoute(filename,route,filepath);
        } 
    });
    
    console.log(chalk.blue.inverse('>') + ' API: ' + chalk.green.bold(path));
}


exports.collector = function() {
    
   
    // Создаем директории в модулях
    fs.readdirSync(PATH_MODULES).forEach(function(f) {
        try {
            var path = __dirname + '/../modules/' + f;
            var stats = fs.lstatSync(path);
            if (stats.isDirectory()) {
                file.mkdirSync(path+'/client/Controllers');
                file.mkdirSync(path+'/client/Service');
                file.mkdirSync(path+'/client/View');
                file.mkdirSync(path+'/server/Controllers');
                file.mkdirSync(path+'/server/Models');
            }
        }
        catch (e) {}
    });
    console.log(chalk.blue.inverse('>') + ' Collector: ' + chalk.green.bold('OK'));
}
exports.indexPage = function(app,path,route){
    var path = path || PATH_TEMPLATE+'/index.jade';
    var route = route || '/';

    app.get(route,function(req,res){
        res.render(path,{});
    });


}
exports.startServer = function(app,port){
    
    

    var server = http.createServer(app);
    
    
    server.listen(port, function() {
        console.log(chalk.blue.inverse('>') + ' Express server listening on port: ' + chalk.green.bold(port));
    });
    
    
    
    //сокеты
    var io = require('socket.io')(server);
    
    io.on('connection', function (socket) {
      socket.emit('news', { hello: 'world' });
      socket.on('my other event', function (data) {
        console.log(data);
      });
    });

}




