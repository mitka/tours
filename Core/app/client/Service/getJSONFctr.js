angular.module('app')
    .factory('getJSONFctr', function($http){
        var config = {
          url:'../categories.json'  
        };
        return {
            setUrl:function(urlParam) {
                config.url = urlParam || config.url;
            },
            fetch:function(callback) {
                console.log(config.url);
                $http.get(config.url).success(callback)
                    .error(function(data, status, headers, config) {
                        console.log('[Error] JSON did not fetch');
                    });
            }
        };
}); 