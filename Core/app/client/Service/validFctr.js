// Валидация
angular.module('app')
    .factory('validFctr',function($http){
   
   
   
		return {

			email: function(email){
				
				var regexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return regexp.test(email);
				
				
			},
			
		};


    });