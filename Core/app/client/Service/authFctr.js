angular.module('app')
    .factory('authFctr',function($http){
   
   
   
		return {
// Авторизация
		    authLogin: function(data,callback){
		        $http.get('/core/user/auth/login',{
                    cache:true,
                    params: {
                        email:data.email,
                        password:data.password
                    }
                })
		        .then(function(responce){
		            return callback(responce.data);
		        });
		    },
// Регистрация
		    authRegister: function(data,callback){
		        $http.get('/core/user/auth/register',{
                    cache:true,
                    params: {
                        email:data.email
                    }
                })
                .then(function(responce){
		            return callback(responce.data);
		        });
		    },
// Выход
		    authLogout: function(callback){
		        $http.get('core/user/auth/exit').then(function(responce){
		            return callback(responce.data);
		        });
		    },

		};


    });