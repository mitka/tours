angular.module('app')
    .controller('sideBarCtrl', function($scope, getJSONFctr) {
        //аккордион
        $scope.activeTabs = [];
        $scope.isOpenTab = function (tab) {
            if ($scope.activeTabs.indexOf(tab) > -1) {
                return true;
            } else {
                return false;
            }
        }
        $scope.openTab = function (tab) {
            if ($scope.isOpenTab(tab)) {
                $scope.activeTabs.splice($scope.activeTabs.indexOf(tab), 1);
            } else {
                $scope.activeTabs.push(tab);
            }
        }
        
        //слайдер
        $scope.value = "10;10000";
        $scope.options = {
            from: 500,
            to: 10000,
            step: 500,
            dimension: ' р.',
            css: {
                background: {
                    "background-color": "#eeeeee"
                },
                before: {
                    "background-color": "purple"
                },
                default: {
                    "background-color": "white"
                },
                after: {
                    "background-color": "#838d8f"
                }
            }
        }

        //получаем категории и формируем меню
        getJSONFctr.fetch(function(res) {
            $scope.categories = {};
            $scope.topCats = [];
            for(var key in res) {
                var arr = [];
                $scope.topCats.push(key);
                for(var i=0; i<res[key].length; i++) {
                    arr.push(res[key][i].name);
                }
                $scope.categories[key] = arr;
            }
        });
    });