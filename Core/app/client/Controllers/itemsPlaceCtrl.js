angular.module('app')
    .controller('itemsPlaceCtrl', function($scope, $rootScope, getJSONFctr) {

    	$scope.showItems = function(topCat, category) {
    		console.log('getJSONFctr');
    		var getJSON = new getJSONFctr();
    		getJSON.setUrl('../items.json');
    		getJSON.fetch(function(res){
    			$scope.items = res[topCat][category];
    		});
    		console.log($scope.items);
    	}
    });
