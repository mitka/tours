angular.module('app')
    .controller('lkToolbarCtrl',function($scope,$rootScope,$window,ngDialog){

		$scope.clickAuth = function(){
			$scope.href = '#';
			if(!!!$rootScope.user)
				ngDialog.open({
				    template: '/tmp/app/client/View/modalAuthorization.tpl.html',
				    className: 'ngdialog-theme-simple'
				});
			else
			$scope.href = '/lk';
		};

    });