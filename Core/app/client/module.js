angular.module('app', [
    'ngLocale', // Локализация
    'ngRoute',
    'ngSlider',
    'angular-loading-bar',
    'ngDialog',
    'datePicker' // контрол дат
])
    .config(function($routeProvider, $locationProvider){
        $routeProvider
            .when('/', {
                templateUrl: '/tmp/app/client/View/main.tpl.html'
            })
            .when('/lk', {
                templateUrl: '/tmp/app/client/View/lk.tpl.html'
            })
            .when('/login', {
                templateUrl: '/tmp/app/client/View/login.tpl.html'
            })
            .when('/signup', {
                templateUrl: '/tmp/app/client/View/signup.tpl.html'
            })
            .when('/forgot', {
                templateUrl: '/tmp/app/client/View/forgot.tpl.html'
            })
            .when('/categories', {
                templateUrl: '/tmp/app/client/View/categories.tpl.html'
            })
            .when('/item', {
                templateUrl: '/tmp/app/client/View/item.tpl.html'
            })
            .otherwise({
                redirectTo: '/'
            });
            
            
            
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });