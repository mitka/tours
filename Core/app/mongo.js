var mongoose = require('mongoose');
var chalk = require('chalk');

//////////////////////
var path = require('path');
////////////////////////
	    

var mongolab = 'mongodb://m:123456789@dbh43.mongolab.com:27437/magazine';
  


mongoose.connect(mongolab);
var db = mongoose.connection;

db.on('error', function (err) {
    console.log(chalk.red.inverse('>') + ' Admin express server listening on port: ' + chalk.red.bold('Error'));
});

db.once('open', function callback () {
    console.log(chalk.blue.inverse('>') + ' Connected to mongo: ' + chalk.green.bold('OK'));
});

module.exports = mongoose;