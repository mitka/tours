/*
    Категории товаров
*/

var mongoose      = require(PATH_APP+'mongo');
var _             = require('underscore');
//var async         = require('async');


var categorieSchema = mongoose.Schema({
    parent_id: 'String',
    name: 'String',
    meta_title: 'String',
    meta_keywords: 'String',
    meta_description: 'String',
    description: 'String',
    url: 'String',
    position: 'String',
    visible: 'String'
});

var Categorie = mongoose.model('categorie', categorieSchema);



module.exports.method = function(){};