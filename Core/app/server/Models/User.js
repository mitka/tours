/*
   Пользователи
*/
var passport       = require('passport');
var LocalStrategy  = require('passport-local').Strategy;

var mongoose      = require(PATH_APP+'mongo');
var $email         = require(PATH_LIBS+'/email.js');
var _             = require('underscore');
var crypto        = require('crypto');

var $f            = require(PATH_LIBS+'/$f.js');
//var async         = require('async');

var userSchema = mongoose.Schema({
    email: 'String',
    password: 'String',
    passwordSalt: 'String',

    hash: 'String',

    
    role: 'String',
    
    active: 'Boolean',
    name:{
        firstname: 'String',
        lastname: 'String'
    },
    
    created: 'Date'
});

var User = mongoose.model('user', userSchema);

module.exports.model = User;


/*
##############################
          PasportJS
##############################
*/

passport.use('local',
    new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function (username, password, done) {
        if(!!username && !!password)
            User.findOne({ email : username},function(err,user){
                if(!!!err && !!user){
                    
                    password = crypto.createHash('sha1')
                        .update(password+user.passwordSalt)
                        .digest('hex');
                    
                    if(user.password == password)
                        return done(null,user);
                    else return done(null, false);
                }
                else return done(err);
            });
        else {
            return done({err:'Поле электронная почта или пароль не введены'}, false);
        }
    })
);


passport.serializeUser(function(user, done) {
    done(null, user.id);
});


passport.deserializeUser(function(id, done) {
    User.findById(id, function(err,user){
      err 
        ? done(err)
        : done(null,user);
    });
});               
  
/*
#######################
*/


/*
    Авторизация
*/

var Authorization = function(){
    this.salt = crypto.createHash('sha1')
    .update(''+new Date())
    .digest('hex');
    
}


// Регистрация
Authorization.prototype.register = function(email, callback) {
    var self = this;
    

    var password = $f.gen_pass();
    if(!!email){
        
        password = crypto.createHash('sha1')
        .update(password+self.salt)
        .digest('hex');
        var hash = $f.UUID();
        
        User.findOne({email:email},function(err,data){
            if(!!!data){
                var user = new User({ 
                    email: email, 
                    password: password, 
                    passwordSalt:self.salt,
                    active:false,
                    hash: hash
                });

                $email.sendMailRegistration(email,password,hash);

                user.save(function(err) {
                    return err
                        ? callback(err)
                            : callback(null,user);
                });
            }
            else{
                callback({err:'Электронная почта уже зарегистрирована'},null);
            }
        });
        
        
        
    }
    
    else callback({err:'Электронная почта не введена'});
};





// Авторизация
Authorization.prototype.login = function(email, password, callback) {
    var self = this;
    callback({err:true},null);
};

Authorization.prototype.getUser = function(data){
    var data = JSON.parse(JSON.stringify(data));


    delete data.__v;
    delete data.activated;
    delete data.created;
    delete data.hashs;
    delete data.hash;
    delete data.passwordSalt;
    delete data.password;

    var email = !!data.email ? data.email.split('@')[0] : '';

    data.login = !!data.name ? (!!data.name.first || !!data.name.last ? data.name.first+' '+data.name.last : email) : email;



    return data;
}





/*
    Экспорт
*/
module.exports.Authorization = Authorization;
module.exports.register = function(email, password, callback) {
    new Authorization().register(email, password, callback);
};
module.exports.login = function(email, password, callback) {
    new Authorization().login(email, password, callback);
};
