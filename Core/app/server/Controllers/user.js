// /core/app/server/user/

var User = require(PATH_APP+'server/Models/User');
var passport = require("passport");

module.exports = [
    {
        //params:[':id'],
        //preload:[fun,fun2],
        method: 'get',
        link: function(req,res){
            res.send('1');
        }
    },
// Авторизация
{
    params:['auth/login'],
    //preload: [passport.authenticate('local')],
    method: 'get',
    link: function(req,res,next){
        var Authorization = new User.Authorization();
        
        passport.authenticate('local',
            function(err, user, info) {
              return err 
                ? next(err)
                : user
                  ? req.logIn(user, function(err) {
                      return err
                        ? next(err)
                        : res.send(Authorization.getUser(user));
                    })
                  : res.send({err: 'Неверно введен логин или пароль'});
            }
          )(req, res, next);
    }
},
//  Регистрация
{
    params:['auth/register'],
    method: 'get',
    link: function(req,res){
        
        User.register(req.query.email,function(err,user){
            res.send(err || user);
        });
    }
},






{
    params:['auth/exit'],
    //preload:[fun,fun2],
    method: 'get',
    link: function(req,res){
         console.log('exit',req.user);
        res.send('3');
    }
}];