var nconf = require('nconf');
var mail = require("nodemailer").mail;

var swig  = require('swig');



var from = "Робот магазина <robot@cosmeticufa.com/>"







module.exports.send = function(email,template){}




module.exports.sendMailRecovery = function(email,hash){
	var template = swig.compileFile(PATH_TEMPLATE+'/email/confirm.html');
	var output = template({
		hash: hash,
		email: email
	});

	mail({
		from: from, 
		to: email, 
		subject: "cosmeticufa.com: Восстановления пароля", 
		text: "Восстановления пароля на cosmeticufa.com ✔", 
		html: output
	});
}

module.exports.sendMailRegistration = function(email,password,hash){
	var template = swig.compileFile(PATH_TEMPLATE+'/email/register.html');
	var output = template({
		//hash: hash,
		password: password
	});

	mail({
		from: from, 
		to: email, 
		subject: "cosmeticufa.com: Регистрация на сайте", 
		text: "Регистрация на cosmeticufa.com ✔", 
		html: output
	});
}

module.exports.sendMailNewPassword = function(email,password){
	var template = swig.compileFile(__dirname+'/../views/__email/repassword.html');
	var output = template({
		password: password
	});

	mail({
		from: from,
		to: email, 
		subject: "cosmeticufa.com: Регистрация на сайте", 
		text: "Регистрация на cosmeticufa.com ✔", 
		html: output
	});
}