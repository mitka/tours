
// Перевод рус -> rus
module.exports.translit = function(value,flag){
	if(!!!value) return false;
	var transl=new Array();
    transl['А']='A';     transl['а']='a';
    transl['Б']='B';     transl['б']='b';
    transl['В']='V';     transl['в']='v';
    transl['Г']='G';     transl['г']='g';
    transl['Д']='D';     transl['д']='d';
    transl['Е']='E';     transl['е']='e';
    transl['Ё']='Yo';    transl['ё']='yo';
    transl['Ж']='Zh';    transl['ж']='zh';
    transl['З']='Z';     transl['з']='z';
    transl['И']='I';     transl['и']='i';
    transl['Й']='';      transl['й']='';
    transl['К']='K';     transl['к']='k';
    transl['Л']='L';     transl['л']='l';
    transl['М']='M';     transl['м']='m';
    transl['Н']='N';     transl['н']='n';
    transl['О']='O';     transl['о']='o';
    transl['П']='P';     transl['п']='p';
    transl['Р']='R';     transl['р']='r';
    transl['С']='S';     transl['с']='s';
    transl['Т']='T';     transl['т']='t';
    transl['У']='U';     transl['у']='u';
    transl['Ф']='F';     transl['ф']='f';
    transl['Х']='X';     transl['х']='x';
    transl['Ц']='C';     transl['ц']='c';
    transl['Ч']='Ch';    transl['ч']='ch';
    transl['Ш']='Sh';    transl['ш']='sh';
    transl['Щ']='Shh';   transl['щ']='shh';
    transl['Ъ']='';      transl['ъ']='';
    transl['Ы']='Y';   	 transl['ы']='y';
    transl['Ь']='';    	 transl['ь']='';
    transl['Э']='E';   	 transl['э']='e';
    transl['Ю']='Yu';    transl['ю']='yu';
    transl['Я']='Ya';    transl['я']='ya';
	
	if(!!flag) transl[' ']='_'; 

    var result='';
    for(i=0;i<value.length;i++) {
        if(transl[value[i]]!=undefined) { result+=transl[value[i]]; }
        else { result+=value[i]; }
    }
    
    return !!flag ? result.toLowerCase() : result;
	
}


module.exports.UUID = (function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
  }
  return function() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
  };
})();


module.exports.gen_pass = function() {
    var low = new Array('', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    var dig = new Array('', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    var pswrd = '';
    var znak, s;
    var k = 0;
    var n = 6;
    var pass = new Array();
    var w = rnd(30, 80, 100);
    for (var r = 0; r < w; r++) {

        znak = rnd(1, 26, 100);
        pass[k] = low[znak];
        k++;
    }
    for (var i = 0; i < n; i++) {
        s = rnd(1, k - 1, 100);
        pswrd += pass[s];
    }
    return pswrd;
}


function rnd(x, y, z) {
    var num;
    do {
        num = parseInt(Math.random() * z);
        if (num >= x && num <= y) break;
    } while (true);
    return (num);
}




