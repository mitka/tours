var express = require('express');
var app = express();
var config = require('./Core/app/config');
//var admin = require('./Core/admin/start');
var nconf = require('nconf');


var socket = require('./Core/app/socket');

socket.start(app);

nconf.argv().env().file({ file: __dirname+'/Core/app/nconf.json' });

config.app(express, app);

config.api(app,['modules/**/server/Controllers/*.js'],'/api/');            // api для модулей
config.api(app,['app/server/Controllers/*.js'],'/core/');                  // api для core

config.templateAngular(app);    // Шаблоны для AngularJS
config.collector();             // Кастомный сборщик
config.indexPage(app);          // Главная страница
config.startServer(app,8080);   // Запускаем сервер

config.route(app);        // Роуты для Angular

//admin.start(9999);              // Запускаем админа на другом порту

