module.exports = function(grunt) {
    grunt.initConfig({

        concat: {
            dist: {
                src: [
                    'Core/libs/client/vendor/*.js',
                    'Core/libs/client/angular.js',
                    '**/client/module.js',
                    '**/client/Service/*.js',
                    '**/client/Controllers/*.js',
                    '**/client/other/*.js',
                    '!Core/admin/client/**/*.js'
                ],
                dest: 'public/js/build.js',
            },
            admin:{
                src: [
                    'Core/libs/client/vendor/*.js',
                    'Core/admin/libs/vendor/js/*.js',
                    'Core/admin/client/angular.js',
                    'Core/admin/client/module.js',
                    'Core/admin/client/Controllers/*.js',
                    'Core/admin/client/Service/*.js',
                ],
                dest: 'public/admin/js/build.js',
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            dist: {
                files: {
                    // 'public/js/build.js': ['public/js/build.js']
                }
            }
        },
        less: {
            production: {
                options: {
                    //paths: ['src/less'],
                    cleancss: true
                },
                files: {
                    "public/stylesheets/style.min.css": ["Core/libs/client/styles/*.less", 
                                                        "Core/modules/**/client/**/*.less", 
                                                        "Core/template/styles/styles.less",
                                                        "Core/app/client/View/styles/*.less"]
                }
            },
            admin:{
                options: {
                    //paths: ['src/less'],
                    cleancss: true,
                    strictMath: true,
                    sourceMap: false
                },
                files: {
                    "public/admin/stylesheets/style.min.css": ["Core/admin/libs/vendor/styles/*.less", 
                                                                "Core/admin/template/styles/styles.less",
                                                                "Core/admin/client/View/styles/*.less"]
                }
            }

        },
        imagemin: {
            options: {
                cache: false
            },

            dist: {
                files: [{
                    expand: true,
                    cwd: 'Core/template/img/',
                    src: ['**/*.{png,jpg,gif,svg}'],
                    dest: 'public/img/'
                }]
            },
            admin:{
                files: [{
                    expand: true,
                    cwd: 'Core/admin/template/img/',
                    src: ['**/*.{png,jpg,gif,svg}'],
                    dest: 'public/admin/img/'
                }]
            }
        },
        watch: {
            //при изменении любого из этих файлов запустить задачу 'reload'
            concat:{
                files: [
                    '**/client/**/*.js',
                    '**/client/*.js',
                ],
                tasks: ['concat', 'uglify']
            },
            less: {
                files: ['**/*.less'],
                tasks: ['less'],
            }
            
        }


    });

    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-ng-build');



    grunt.registerTask('default', ['concat', 'uglify', 'less','imagemin']);
    //grunt.registerTask('default', ['less', 'jshint', 'ngbuild', 'concat', 'uglify', 'imagemin']);

};